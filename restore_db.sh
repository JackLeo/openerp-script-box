#!/bin/bash

#We measure script execution time
START=$(date +%s)

BACKUP_FILE=$1
DBNAME=$2
DBOWNER=$3

# Check if we have 3 arguments
if [ $# != 3 ]; then
	echo "Usage: /restore_db.sh /path/to/backup/file.gz new_db_name db_owner"
	exit 1
fi

# Script must be run by postgres
if [ "$USER" != "postgres" ]; then
        echo "Must be run as 'postgres' username"
        exit 1
fi

#check if db exists and ask if we drop it
if psql $DBNAME -c '\q' 2>&1; then
	echo "Database '$DBNAME' exists. Do you want to drop it?"
	select yn in "Yes" "No"; do
		case $yn in
				Yes ) echo "Ok then, droping it..."; dropdb $DBNAME; break;;
				No ) echo "Not dropping. Exiting"; exit 1;;
		esac
	done
else 
	echo "DB does not exists. Let's create it"
fi

# Create db
echo "Creating database $DBNAME ..."
createdb -O $DBOWNER $DBNAME

# Couldn't create db. Exit
psql_exit_status=$?
if [ $psql_exit_status != 0 ]; then
	echo "Error while trying to restore dabatase! Exiting" 1 >&2
	exit 1
fi

# Restore backup file to db
echo "Restoring data from backup file ..."
zcat "$BACKUP_FILE" | psql $DBNAME > /dev/null 2>&1

# Change ownership
echo "Changing tables' ownership to $DBOWNER ..."
for tbl in `psql -qAt -c "select tablename from pg_tables where schemaname = 'public';" $DBNAME` ; do  psql -c "alter table $tbl owner to $DBOWNER" $DBNAME > /dev/null 2>&1 ; done

echo "Changing sequences' ownership to $DBOWNER ..."
for tbl in `psql -qAt -c "select sequence_name from information_schema.sequences where sequence_schema = 'public';" $DBNAME` ; do  psql -c "alter table $tbl owner to $DBOWNER" $DBNAME > /dev/null 2>&1 ; done

echo "Changing views ownership to $DBONNER ..."
for tbl in `psql -qAt -c "select table_name from information_schema.views where table_schema = 'public';" $DBNAME` ; do  psql -c "alter table $tbl owner to $DBOWNER" $DBNAME > /dev/null 2>&1 ; done

# Let's calculate how long it took us
END=$(date +%s)
DIFF=$(( $END - $START ))
echo "Total time:"
printf ""%dh:%dm:%ds"\n" $(($DIFF/3600)) $(($DIFF%3600/60)) $(($DIFF%60))
